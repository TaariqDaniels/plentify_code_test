/*
 * File:   m90e26.h
 * Author: Candidate Name
 */

#ifndef M90E26_H
#define	M90E26_H

typedef const struct
{
    void (*initialise) (void);
}m90e26_interface_t;

extern m90e26_interface_t m90e26;

#endif	/* M90E26_H */
