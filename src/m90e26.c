/*
 * File:   m90e26.c
 * Author: Taariq Daniels
 */

 #include "m90e26.h"
 #include "uart.h"

// Function prototypes
static void m90e26_initialise(void);

m90e26_interface_t m90e26 =
{
    .initialise = m90e26_initialise,
};

static void m90e26_initialise(void)
{

//initialise UART
 uart_interface_t uart.enable(uart_t p_uartx); 
 uart_interface_t uart.enable_rx_module(uart_t p_uartx);
 uart_interface_t uart.enable_tx_module(uart_t p_uartx);
 uart_interface_t uart.set_baudrate(p_uart2, uart_baudrate_9600);  //baudrate = 9600hz
 uart_interface_t uart.select_data_and_parity(p_uart2, uart_data_and_parity_select_8bit_no_parity); //8 bit no parity
 uart_interface_t uart.run_during_sleep(uart_t p_uartx, True);
 uart_interface_t uart.select_clock(uart_t p_uartx, uart_clock_select_sysclk); //using system clk
 uart_interface_t uart.select_clock(uart_t p_uartx, uart_flow_control_pin_enable_rts_cts);
 uart_interface_t uart.select_baudrate_speed_mode(uart_t p_uartx, uart_baudrate_speed_mode_standard);
 uart_interface_t uart.select_stop_bits(uart_t p_uartx, uart_stop_bit_select_1bit); 
 uart_interface_t uart.select_transmit_interrupt_mode(uart_t p_uartx, uart_transmit_interrupt_select_all_characters_transmitted);
 uart_interface_t uart.select_receive_interrupt_mode(uart_t p_uartx, uart_receive_interrupt_select_three_characters_received;
    
 //initialise GPIO
 gpio_interface_t gpio.initialise(m90e26_uart_tx,gpio_mode_digital_output_push_pull);
 gpio_interface_t gpio.initialise(m90e26_uart_rx,gpio_mode_digital_input_pull_up);
 
 
 
 
 